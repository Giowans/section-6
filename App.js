import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';
import Components from './src/screens/componentScreen';
import ListitaFlat from './src/screens/ListScreen';
import ImageScreen from './src/screens/ImageScreen';
import CounterScreen from './src/screens/CounterScreen';
import ColorScreen from './src/screens/ColorScreen';
import RgbScreen  from './src/screens/RgbScreen';
import TextScreen from './src/screens/TextScreen';

const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    Componentes: Components,
    ListScreen: ListitaFlat,
    ImgScreen: ImageScreen,
    Counter: CounterScreen,
    Colors: ColorScreen,
    RGB: RgbScreen,
    InputTextito: TextScreen
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'Sección 6'
    }
  }
);

export default createAppContainer(navigator);
