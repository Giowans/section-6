# Sección 6: Estados y Reductores

Esta ha sido de las seciones mas intensas del curso hasta ahora: y es que involucra bastante teoría. Dentro de todas las actividades vistas en esta sección
creo que lo más importante que podemos destacar es el uso de dos nuevos componentes apenas introducidos: **useState** y **useReducer** los cuales pertenecen
a la libreria de `react`. 

Básicamente estos dos componentes de comportamiento nos ayudan a manejar estados dentro de nuestra aplicación para poder renderizar componentes y datos en tiempo
real al accionar un evento como clickear un botón o cambiar el texto de un _InputText_ (spoilers).

### Estados (useState):

Este componente nos permite _agregarle_ (por así decirlo) un estado de comportamiento a nuestro componente o _screen_ que actúa en base a una variable (factor) 
dentro de el/ella. las variables por si solas, al ser renderizadas en nuestra app, solamente se renderizan una vez: internamente su valor cambia (en memoria) sin
embargo esto no es visible para el usuario. Es por eso que usamos un estado (**useState**), para decirle a React: "Hey, cada vez que se modifique el valor tambien
modifica a tus componentes, o sea, renderizate nuevamente".

```jsx
 const [factor, cambioEnElFactor] = useState(valorInicialDelFactor);
```

Esta es la sintaxis general cuando queremos añadir un estado a nuestro componente: cada vez que alteremos su valor lo haremos con la función `cambioEnElFactor` para que
React pueda renderizar el componente nuevamente una vez haya cambiado el valor.

### Reductores (useReducer):

Este componente nos permite generar un **conjunto de factores** que reaccionen a un mismo método. El objetivo principal es ayudarnos a manejar una mejor estructura
de código. 
Supongamos que en mi componente existen muchos factores que reaccionen a un mismo método de cambio, o sea, que se operen de igual manera, por ejemplo: queremos mostrar la cantidad
existente de 15 productos en venta. En si todos son contadores: lo unico que hacen es decrementar o incrementar su valor de existencia, entonces, no es una muy buena idea
generar 15 estados independientes para cada factor. 
Aquí es donde entra **useReducer**, a un estado le asigna un conjunto de factores que pueden cambiar el conexto de la aplicación. A diferencia de **useState** este necesita
en sus parámetros un apuntador a la función que hará las operaciones para que cada vez que esta sea usada React detecte que necesita renderizar nuestro componente otra vez.

```jsx
const operandoFactores = (conjuntoDeFactores, acciones) =>
{
    //código que opera los factores segun  las acciones
}

 const [conjuntoDeFactoes, disparador] = useState(operandoFactores, {factor1: valorInicial, factor2: valorInicial, factorN: valorInicial});
```

Cada vez que queramos hacer un cambio a algun factor basta con llamar al `disparador` y mandarle los parametros correctos para que opere de manera correcta: Esta estructura del reductor nos ayuda a no hacer
_código Spaghetti_ :p

### Notas:

- En el repositorio de esta sección se añadieron _Screens_ y _Componentes_ que ilustran mejor lo antes descrito: `ColorScreen.js` y `RgbScreen.js` son ejemnplo de estas actividades.
- Se nos introdujo el componente primitivo _InputText_ en la actividad de `TextScreen.js`. Este componente pertenece a la libreria `react-native` y concluimos que su correcto funcionamiento depende
del uso de un estado (**useState**)

