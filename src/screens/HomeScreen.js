import React from 'react';
import { Text, StyleSheet, View, Button, TouchableOpacity} from 'react-native';

const HomeScreen = ({navigation}) => {
  return(
    <View>
      <Text style={styles.text}>Botones con Navegacion :D</Text>
      <Button
        title = "Vamos a los componentes :D" 
        onPress = {()=> navigation.navigate('Componentes')}
      />
      <Button
        title = "Vamos a la Listita" 
        onPress = {()=> navigation.navigate('ListScreen')}
      />
      <Button
        title = "Vamos a la ImageScreen" 
        onPress = {()=> navigation.navigate('ImgScreen')}
      />
      <Button
        title = "Vamos a el CounterScreen" 
        onPress = {()=> navigation.navigate('Counter')}
      />
      <Button
        title = "Vamos a el ColorScreen" 
        onPress = {()=> navigation.navigate('Colors')}
      />
      <Button
        title = "Vamos a el RgbScreen" 
        onPress = {()=> navigation.navigate('RGB')}
      />
      <Button
        title = "Vamos a el TextScreen" 
        onPress = {()=> navigation.navigate('InputTextito')}
      />                        
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;
