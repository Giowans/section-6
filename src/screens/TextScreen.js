import React, {useState} from 'react';
import {TextInput, View, StyleSheet, Text} from 'react-native';

const TextScreen = () =>
{
    const [name, setName] = useState('');
    const [pass, setPass] = useState('');
    return(
        <View>
            <Text style={{fontSize: 18}}>Ingresa tu Nombre:</Text>
            <TextInput 
                style = {styles.inputStyle}
                autoCapitalize= "none"
                autoCorrect = {false}
                onChangeText = {(newValue)=>setName(newValue)}
                value = {name}
            />
            <Text style={{fontSize: 18}}>Mi nombre es: {name}</Text>
            <Text style={{fontSize: 18}}>Ingresa tu contraseña: {pass.length}</Text>
            <TextInput 
                style = {styles.inputStyle}
                autoCapitalize= "none"
                autoCorrect = {false}
                onChangeText = {(newValue)=>
                    {
                        setPass(newValue);
                    }}
                value = {pass}
            />
            {pass.length > 4
                ? null
                : <Text style={{fontSize: 14, color: 'red'}}>La contraseña debe de tener al menos 5 caracteres</Text>}
        </View>
    );
};

const styles = StyleSheet.create({
    inputStyle:{
        margin: 15,
        borderWidth: 2,
        borderColor: 'gold',
        borderRadius: 20,
    }
});

export default TextScreen;