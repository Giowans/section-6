import React, {useReducer} from 'react';
import {View, Button, Text, StyleSheet} from 'react-native';

const reducer = (state, action) =>
{
    //state === {counter: number}
    //action === {type = 'increment' || 'decrement', payload: 1}

    switch(action.type)
    {
        case 'increment':
            return {...state, contador: state.contador + action.payload};
        case 'decrement':
            return {...state, contador: state.contador - action.payload};
    }
};
const CounterScreen = () =>
{
    const [state, dispatch] = useReducer(reducer, {contador: 0});
    return(
        <View>
            <Button
                title="Incrementar" style = {style.buttonStyle}
                onPress = {()=>{
                    dispatch({type: 'increment', payload: 1});
                }}
            />
            <Button 
                title="Decrementar" style = {style.buttonStyle}
                onPress = {()=>{
                    dispatch({type: 'decrement', payload: 1});
                }}
            />
            <Text style={style.textStyle}> Contador: {state.contador}</Text>
        </View>
    );
};

const style = StyleSheet.create({
    buttonStyle:
    {
        width: 50,
        height: 50,
        paddingTop: 20,
        backgroundColor: 'red'
    },
    textStyle:
    {
        fontSize: 24,
        textAlign: "center",
        padding: 10
    }
});

export default CounterScreen;