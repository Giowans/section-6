import React from 'react';
import {View, Button, Text, StyleSheet} from 'react-native';

const ColorChanger = ({colorName, increaseColor, decreaseColor}) =>
{
    return(
        <View>
            <Text style={{fontSize: 24, textAlign: "center", paddingTop: 10}}>{colorName}</Text>
            <Button
                title = {`Incrementar ${colorName}`}
                onPress = { ()=> increaseColor()}
                style={{alignContent: "center", }}
            />
            <Button
                title = {`Decrementar ${colorName}`}
                onPress = { ()=> decreaseColor()}
                style={{alignContent: "center", }}
            />
        </View>
    );
};

const style = StyleSheet.create({});

export default ColorChanger;