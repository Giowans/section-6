import React from 'react';
import {Text, StyleSheet, Image, View} from 'react-native';

const ImageDetails = (props) => {
    return (
        <View>
            <Image source= {props.imgSource} style = {styles.imgStyle}></Image>
            <View>
                <Text style= {styles.textStyle}>{props.titulo}{'\n'}
                    <Text style= {{backgroundColor: 'khaki'}}>Score: {props.score}</Text>
                </Text>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    textStyle:{
        textAlign: "center",
        fontSize: 20,
        backgroundColor: "aquamarine"
    },
    imgStyle:{
        alignContent: "flex-start",
        paddingTop: 10
    }
});

export default ImageDetails;